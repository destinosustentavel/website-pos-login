import React from 'react';
import { BrowserRouter, Route } from 'react-router-dom';

import Home from './pages/Home';
import Feed from './pages/Feed';
import Notifications from './pages/Notifications';

import MyMessages from './pages/Chat/MyMessages';
import NewChat from './pages/Chat/NewChat';

import MyDemand from './pages/Demand/MyDemand';
import NewDemand from './pages/Demand/NewDemand';
import AttendedDemand from './pages/Demand/AttendedDemand';

import MyOffers from './pages/Offers/MyOffers';
import NewOffer from './pages/Offers/NewOffer';
import AttendedOffers from './pages/Offers/AttendedOffers';

import MyRanking from './pages/Ranking/MyRanking';
import RankingCategory from './pages/Ranking/RankingCategory';

import Profile from './pages/Profile';
import Settings from './pages/Settings';
import Details from './pages/Details';

export default function Routes() {
  return (
    <BrowserRouter>
      <Route path="/" exact component={Home} />
      <Route path="/feed" component={Feed} />
      <Route path="/notificacoes" component={Notifications} />

      <Route path="/minhas-mensagens" component={MyMessages} />
      <Route path="/nova-conversa" component={NewChat} />

      <Route path="/meus-pedidos" component={MyDemand} />
      <Route path="/novo-pedido" component={NewDemand} />
      <Route path="/pedidos-atendidos" component={AttendedDemand} />

      <Route path="/minhas-ofertas" component={MyOffers} />
      <Route path="/nova-oferta" component={NewOffer} />
      <Route path="/ofertas-atendidas" component={AttendedOffers} />

      <Route path="/minha-posicao" component={MyRanking} />
      <Route path="/ranking-categoria" component={RankingCategory} />

      <Route path="/perfil" component={Profile} />
      <Route path="/configuracoes" component={Settings} />
      <Route path="/detalhes" component={Details} />
    </BrowserRouter>
  );
}