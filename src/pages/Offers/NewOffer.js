import React from 'react';

import Container from '../../components/Container';

export default function NewOffer() {
  return (
    <Container>
      <div class="inner-block">
        <div class="inbox">
          <h2>Nova Oferta </h2>

          <div class="col-md-12 compose-right">
            <div class="inbox-details-default">
              <div class="inbox-details-heading">
                Cadastrar Nova Oferta
              </div>

              <div class="inbox-details-body">
                <div class="alert alert-info">
                  Por Favor, Verifique todos os detalhes antes de enviar!
							  </div>

                <form class="com-mail" action="c_nova_oferta_pedido.php" method="post" class="novo_ped_ofert">
                  <select name="material" id="material" class="caixaSelect" onChange="showDiv(this.value);">
                    <option value="plano0" selected disabled>&nbsp;SELECIONE O TIPO DE MATERIAL:&nbsp;</option>
                    <option value="plano1" >&nbsp;PAPEL&nbsp;</option>
                    <option value="plano2">&nbsp;PLÁSTICO&nbsp;</option>
                    <option value="plano3">&nbsp;METAL&nbsp;</option>
                    <option value="plano4">&nbsp;VIDRO&nbsp;</option>
                    <option value="plano5" >&nbsp;ELETRÔNICO&nbsp;</option>
                    <option value="plano6" >&nbsp;ORGÂNICO&nbsp;</option>
                  </select>

                  <div id="plano0" class="invisivel"></div>

                  {/* PAPEL */}
                  <div id="plano1" class="invisivel">
                    <select name="tipo_material" id="material" class="caixaSelect">
                      <option selected disabled >&nbsp;SELECIONE O SUBTIPO DE MATERIAL:&nbsp;</option>
                      <option value="cadernos">&nbsp;CADERNOS&nbsp;</option>
                      <option value="cartazes">&nbsp;CARTAZES&nbsp;</option>
                      <option value="embalagens">&nbsp;EMBALAGENS&nbsp;</option>
                      <option value="envelopes">&nbsp;ENVELOPES&nbsp;</option>
                      <option value="folhasA4">&nbsp;FOLHAS A4&nbsp;</option>
                      <option value="fotocópias">&nbsp;FOTOCÓPIAS&nbsp;</option>
                      <option value="jornal">&nbsp;JORNAL&nbsp;</option>
                      <option value="papelão">&nbsp;PAPELÃO&nbsp;</option>
                      <option value="papel de fax">&nbsp;PAPEL DE FAX&nbsp;</option>
                      <option value="revistas">&nbsp;REVISTAS&nbsp;</option>
                      <option value="outro">&nbsp;OUTRO&nbsp;</option>
                    </select>

                    <input type="text" name="quantidade_material" value="QUANTIDADE :" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Quantidade';}" />

                    <select name="tipo_quantidade" class="caixaSubQuant">
                      <option selected disabled >SELECIONE UMA UNIDADE DE MEDIDA: &nbsp;</option>
                      <option value="g">&nbsp;GRAMAS&nbsp;</option>
                      <option value="mL">&nbsp;MILIGRAMAS&nbsp;</option>
                      <option value="Kg">&nbsp;QUILOS&nbsp;</option>
                      <option value="rl">&nbsp;ROLOS&nbsp;</option>
                      <option value="un">&nbsp;UNIDADES&nbsp;</option>
                    </select>

                    <input type="text" name="prazo_atende" value="PRAZO DE ATENDIMENTO : (EM DIAS)" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Prazo';}" />
                    <textarea rows="6" name="descricao" value="DESCRIÇÃO DO MATERIAL:" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Descrição';}"></textarea>

                    <div>
                      <input type="submit" value="ENVIAR OFERTA" />
                      <input type="button" value="CANCELAR" onclick="location.href='inicial_logado.php' " />
                    </div>
                  </div>
                </form>

                {/* PLASTICO */}
                <form class="com-mail" action="c_nova_oferta_pedido.php" method="post">
                  <div id="plano2" class="invisivel">
                    <select name="material" id="material" class="invisivel" onChange="showDiv(this.value);">
                      <option value="plano2" selected>&nbsp;PLÁSTICO&nbsp;</option>
                    </select>

                    <select name="tipo_material" class="caixaSubtipoMat">
                      <option selected disabled>&nbsp;SELECIONE O SUBTIPO DE MATERIAL:&nbsp;</option>
                      <option value="brinquedos">&nbsp;BRINQUEDOS&nbsp;</option>
                      <option value="canetas">&nbsp;CANETAS&nbsp;</option>
                      <option value="copos">&nbsp;COPOS&nbsp;</option>
                      <option value="embalagens">&nbsp;EMBALAGENS&nbsp;</option>
                      <option value="garrafas">&nbsp;GARRAFAS&nbsp;</option>
                      <option value="potes">&nbsp;ISOPOR&nbsp;</option>
                      <option value="potes">&nbsp;POTES&nbsp;</option>
                      <option value="sacos">&nbsp;SACOS&nbsp;</option>
                      <option value="sacolas">&nbsp;SACOLAS&nbsp;</option>
                      <option value="utensilios">&nbsp;UTENSÍLIOS (EX: BALDE)&nbsp;</option>
                      <option value="vasilhas">&nbsp;VASILHAS&nbsp;</option>
                      <option value="outro">&nbsp;OUTRO&nbsp;</option>
                    </select>

                    <input type="text" name="quantidade_material" value="QUANTIDADE :" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Quantidade';}" />

                    <select name="tipo_quantidade" class="caixaSubQuant">
                      <option selected disabled>SELECIONE A UNIDADE:&nbsp;</option>
                      <option value="rl">&nbsp;FOLHAS&nbsp;</option>
                      <option value="g">&nbsp;GRAMAS&nbsp;</option>
                      <option value="mL">&nbsp;MILIGRAMAS&nbsp;</option>
                      <option value="Kg">&nbsp;QUILOS&nbsp;</option>
                      <option value="un">&nbsp;UNIDADES&nbsp;</option>
                    </select>

                    <input type="text" name="prazo_atende" value="PRAZO DE ATENDIMENTO : (EM DIAS)" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Prazo';}" />

                    <textarea rows="6" name="descricao" value="DESCRIÇÃO DO MATERIAL :" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Descrição';}"></textarea>

                    <div>
                      <input type="submit" value="ENVIAR OFERTA" />
                      <input type="button" value="CANCELAR" onclick="location.href='inicial_logado.php' " />
                    </div>
                  </div>
                </form>

                {/* METAL */}
                <form class="com-mail" action="c_nova_oferta_pedido.php" method="post">
                  <div id="plano3" class="invisivel">
                    <select name="material" id="material" class="invisivel" onChange="showDiv(this.value);">
                      <option value="plano3" selected>&nbsp;METAL&nbsp;</option>
                    </select>

                    <select name="tipo_material" class="caixaSubtipoMat">
                      <option selected disabled>&nbsp;SELECIONE O SUBTIPO DE MATERIAL:&nbsp;</option>
                      <option value="embalagens">&nbsp;EMBALAGENS&nbsp;</option>
                      <option value="enlatado">&nbsp;ENLATADOS&nbsp;</option>
                      <option value="garrafa">&nbsp;GARRAFAS&nbsp;</option>
                      <option value="latas">&nbsp;LATAS&nbsp;</option>
                      <option value="panelas">&nbsp;PANELAS&nbsp;</option>
                      <option value="papel alum">&nbsp;PAPEL ALUMÍNIO&nbsp;</option>
                      <option value="pregos">&nbsp;PREGOS&nbsp;</option>
                      <option value="talheres">&nbsp;TALHERES&nbsp;</option>
                      <option value="tampas garrafa">&nbsp;TAMPAS DE GARRAFA&nbsp;</option>
                      <option value="tampas panela">&nbsp;TAMPAS DE PANELA&nbsp;</option>
                      <option value="outro">&nbsp;OUTRO&nbsp;</option>
                    </select>

                    <input type="text" name="quantidade_material" value="QUANTIDADE :" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Quantidade';}" />

                    <select name="tipo_quantidade" class="caixaSubQuant">
                      <option selected disabled>SELECIONE A UNIDADE:&nbsp;</option>
                      <option value="g">&nbsp;GRAMAS&nbsp;</option>
                      <option value="L">&nbsp;LITROS&nbsp;</option>
                      <option value="mL">&nbsp;MILIGRAMAS&nbsp;</option>
                      <option value="Kg">&nbsp;QUILOS&nbsp;</option>
                      <option value="rl">&nbsp;ROLOS&nbsp;</option>
                      <option value="un">&nbsp;UNIDADES&nbsp;</option>
                    </select>

                    <input type="text" name="prazo_atende" value="PRAZO DO ATENDIMENTO : (EM DIAS)" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Prazo';}" />
                    <textarea rows="6" name="descricao" value="DESCRICAO DO MATERIAL :" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Descrição';}"></textarea>

                    <div>
                      <input type="submit" value="ENVIAR OFERTA" />
                      <input type="button" value="CANCELAR" onclick="location.href='inicial_logado.php' " />
                    </div>
                  </div>
                </form>

                {/* VIDRO */}
                <form class="com-mail" action="c_nova_oferta_pedido.php" method="post">
                  <div id="plano4" class="invisivel">
                    <select name="material" id="material" class="invisivel" onChange="showDiv(this.value);">
                      <option value="plano4" selected>&nbsp;VIDRO&nbsp;</option>
                    </select>

                    <select name="tipo_material_vidro" class="caixaSubtipoMat">
                      <option selected disabled>&nbsp;SELECIONE O ESTADO DO MATERIAL: &nbsp;</option>
                      <option value="cacos">&nbsp;EM CACOS&nbsp;</option>
                      <option value="inteiro">&nbsp;INTEIRO&nbsp;</option>
                    </select>

                    <select name="tipo_material" class="caixaSubtipoMat">
                      <option selected disabled>&nbsp;SELECIONE O SUBTIPO DE MATERIAL&nbsp;</option>
                      <option value="embalagens">&nbsp;EMBALAGENS&nbsp;</option>
                      <option value="copos">&nbsp;COPOS&nbsp;</option>
                      <option value="frascos">&nbsp;FRASCOS&nbsp;</option>
                      <option value="garrafas">&nbsp;GARRAFAS&nbsp;</option>
                      <option value="potes">&nbsp;POTES&nbsp;</option>
                      <option value="vidros de janelas">&nbsp;VIDROS DE JANELAS&nbsp;</option>
                      <option value="outro">&nbsp;OUTRO&nbsp;</option>
                    </select>

                    <input type="text" name="quantidade_material" value="QUANTIDADE :" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Quantidade';}" />

                    <select name="tipo_quantidade" class="caixaSubQuant">
                      <option selected disabled>SELECIONE UMA UNIDADE:&nbsp;</option>
                      <option value="g">&nbsp;GRAMAS&nbsp;</option>
                      <option value="L">&nbsp;LITROS&nbsp;</option>
                      <option value="mL">&nbsp;MILIGRAMAS&nbsp;</option>
                      <option value="Kg">&nbsp;QUILOS&nbsp;</option>
                      <option value="rl">&nbsp;ROLOS&nbsp;</option>
                      <option value="un">&nbsp;UNIDADES&nbsp;</option>
                    </select>

                    <input type="text" name="prazo_atende" value="PRAZO DE ATENDIMENTO : (EM DIAS)" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Prazo';}" />
                    <textarea rows="6" name="descricao" value="DESCRIÇÃO DO MATERIAL :" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Descrição';}"></textarea>

                    <div>
                      <input type="submit" value="ENVIAR OFERTA" />
                      <input type="button" value="CANCELAR" onclick="location.href='inicial_logado.php' " />
                    </div>
                  </div>
                </form>

                {/* ELETRÔNICO (FALTA TERMINAR) */}
                <form class="com-mail" action="c_nova_oferta_pedido.php" method="post">
                  <div id="plano5" class="invisivel">
                    <select name="material" id="material" class="invisivel" onChange="showDiv(this.value);">
                      <option value="plano5" selected>&nbsp;ELETRÔNICO&nbsp;</option>
                    </select>

                    <select name="tipo_material" class="caixaSubtipoMat">
                      <option selected disabled>&nbsp;SELECIONE O SUBTIPO DE MATERIAL:&nbsp;</option>
                      <option value="camera">&nbsp;CÂMERA FOTOGRÁFICA&nbsp;</option>
                      <option value="computador">&nbsp;COMPUTADOR&nbsp;</option>
                      <option value="impressora">&nbsp;IMPRESSORAS&nbsp;</option>
                      <option value="lampada">&nbsp;LAMPADAS&nbsp;</option>
                      <option value="monitor">&nbsp;MONITOR DE COMPUTADOR&nbsp;</option>
                      <option value="pilha e bateria">&nbsp;PILHAS E BATERIAS&nbsp;</option>
                      <option value="telefone">&nbsp;TELEFONE CELULAR&nbsp;</option>
                      <option value="outro">&nbsp;OUTRO&nbsp;</option>
                    </select>

                    <input type="text" name="quantidade_material" value="QUANTIDADE :" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Quantidade';}" />

                    <select name="tipo_quantidade" class="caixaSubQuant">
                      <option selected disabled>SELECIONE A UNIDADE:&nbsp;</option>
                      <option value="g">&nbsp;GRAMAS&nbsp;</option>
                      <option value="mL">&nbsp;MILIGRAMAS&nbsp;</option>
                      <option value="Kg">&nbsp;QUILOS&nbsp;</option>
                      <option value="un">&nbsp;UNIDADES&nbsp;</option>
                    </select>

                    <input type="text" name="prazo_atende" value="PRAZO DO ATENDIMENTO : (EM DIAS)" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Prazo';}" />
                    <input type="text" name="descricao" value="DESCRICAO DO MATERIAL :" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Descrição';}" />

                    <div>
                      <input type="submit" value="ENVIAR OFERTA" />
                      <input type="button" value="CANCELAR" onclick="location.href='minhas_ofertas.php' " />
                    </div>
                  </div>
                </form>

                {/* ORGÂNICO (FALTA TERMINAR) */}
                <form class="com-mail" action="c_nova_oferta_pedido.php" method="post">
                  <div id="plano6" class="invisivel">
                    <select name="material" id="material" class="invisivel" onChange="showDiv(this.value);">
                      <option value="plano6" selected>&nbsp;ORGÂNICO&nbsp;</option>
                    </select>

                    <select name="tipo_material" class="caixaSubtipoMat">
                      <option selected disabled>&nbsp;SELECIONE O SUBTIPO DE MATERIAL:&nbsp;</option>
                      <option value="vegetais">&nbsp;VEGETAIS&nbsp;</option>
                      <option value="adubo">&nbsp;ADUBO&nbsp;</option>
                      <option value="outro">&nbsp;OUTRO&nbsp;</option>
                    </select>

                    <input type="text" name="quantidade_material" value="QUANTIDADE :" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Quantidade';}" />

                    <select name="tipo_quantidade" class="caixaSubQuant">
                      <option selected disabled>SELECIONE A UNIDADE:&nbsp;</option>
                      <option value="g">&nbsp;GRAMAS&nbsp;</option>
                      <option value="mL">&nbsp;MILIGRAMAS&nbsp;</option>
                      <option value="Kg">&nbsp;QUILOS&nbsp;</option>
                      <option value="un">&nbsp;UNIDADES&nbsp;</option>
                    </select>

                    <input type="text" name="prazo_atende" value="PRAZO DO ATENDIMENTO : (EM DIAS)" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Prazo';}" />
                    <input type="text" name="descricao" value="DESCRICAO DO MATERIAL :" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Descrição';}" />

                    <div>
                      <input type="submit" value="ENVIAR OFERTA" />
                      <input type="button" value="CANCELAR" onclick="location.href='minhas_ofertas.php' " />
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>

          <div class="clearfix"> </div>
        </div>
      </div>
    </Container>
  );
}