import React from 'react';

import Container from '../../components/Container';

export default function MyMessages() {
  return (
    <Container>
      <div className="inner-block">
        <div className="inbox">
          <h2>Minhas Mensagens</h2>
          <div className="col-md-4 compose">
            <div className="mail-profile">
              <div className="mail-pic">
                <a href="#">
                  <img id="imgPerfMens" src="" alt="Thumbnail" />
                </a>
              </div>
              <div className="mailer-name">
                <h5><a href="#">Usuário 1</a></h5>
                <h6><a href="mailto:usuario1">Usuário 1</a></h6>
              </div>
              <div className="clearfix"> </div>
            </div>
            <div className="compose-block">
              <a href="nova_conversa.php">Nova Mensagem</a>
            </div>
            <div className="compose-bottom">
              <nav className="nav-sidebar">
                <ul className="nav tabs">
                  <li className="active"><a href="#tab1\" data-toggle="tab"><i className="fa fa-inbox"></i>Minhas conversas <span>10</span><div className="clearfix"></div></a></li>
                  <li className=""><a href="/nova-conversa"><i className="fa fa-pencil-square-o"></i>Escrever Mensagem <div className="clearfix"></div></a></li>
                </ul>
              </nav>
            </div>
          </div>
          <div className="col-md-8 mailbox-content  tab-content tab-content-in">
            <div className="tab-pane active text-style" id="tab1">
              <div className="mailbox-border">
                <div className="mail-toolbar clearfix">
                  <div className="float-left">
                    <a href="/minhas-mensagens">
                      <div className="btn btn_1 btn-default mrg5R">
                        <i className="fa fa-refresh"> </i>
                      </div></a>
                    <div className="dropdown dropdown-inbox">
                      <a href="#" title="" className="btn btn-default" data-toggle="dropdown" aria-expanded="false">
                        <i className="fa fa-trash"></i>
                        <div className="ripple-wrapper"></div></a>

                    </div>
                    <div className="clearfix"> </div>
                  </div>
                  <div className="float-right">
                    <i className="fa fa-comments"></i>
                    <span className="text-muted m-r-sm">&nbsp;<b>MENSAGENS</b>&nbsp;</span>
                    <i className="fa fa-comments"></i>

                    <div className="clearfix"> </div>
                  </div>
                </div>

                <table classNameName="table tab-border">
                  <tbody>
                    <tr
                      className="unread checked"
                      onclick={() => console.log('bate papo id conversa')}
                      style={{ cursor: 'pointer' }}
                    >
                      <td className="hidden-xs">
                        <input type="checkbox" className="checkbox" />
                      </td>
                      <td className="hidden-xs">
                        <i className="fa fa-envelope"> </i>
                      </td>
                      <td><strong>".$conversa."</strong></td>
                      <td><i className="fa fa-calendar"></i></td>
                      <td><strong>".$data_hr."</strong></td>
                    </tr>
                    <tr>
                      <td>".$conversa."</td>
                      <td><i className="fa fa-calendar"></i></td>
                      <td>".$data_hr."</td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
            <div className="tab-pane text-style" id="tab2">
              <div className="mailbox-border">

                <table className="table tab-border">
                  <tbody>
                    <tr
                      className="unread checked"
                      onClick={() => console.log('bate papo')}
                      style={{ cursor: 'pointer' }}
                    >
                      <td className="hidden-xs">
                        <input type="checkbox" className="checkbox" />
                      </td>
                      <td className="hidden-xs">
                        <i className="fa fa-envelope-open"> </i>
                      </td>
                      <td>".$loginDestinatario."</td>
                      <td><i className="fa fa-calendar"></i></td>
                      <td>".$data_hr."</td></tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>

          <div className="clearfix"> </div>
        </div>
      </div>
    </Container >
  );
}