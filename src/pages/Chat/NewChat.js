import React from 'react';

import Container from '../../components/Container';

export default function NewChat() {
  return (
    <Container>
      <div className="inner-block">
        <div className="inbox">
          <h2>Nova Conversa</h2>
          <div className="col-md-4 compose">
            <div className="mail-profile">
              <div className="mail-pic">
                <a href="#">
                  <img id="imgPerfMens" src="" alt="Thumbnail" />
                </a>
              </div>
              <div className="mailer-name">
                <h6><a href="mailto:info@example.com">info@example.com</a></h6>
              </div>
              <div className="clearfix"> </div>
            </div>
            
            <div className="compose-bottom">
              <ul>
                <li><a href="minhas_mensagens.php"><i className="fa fa-inbox"> </i>Minhas conversas</a></li>
                <li><a className="hilate" href="#"><i className="fa fa-pencil-square-o"> </i>Escrever Mensagem</a></li>
              </ul>
            </div>
          </div>

          <div className="col-md-8 compose-right">
            <div className="inbox-details-default">
              <div className="inbox-details-heading">Escrever Nova Mensagem</div>
              <div className="inbox-details-body">
                <div className="alert alert-info">
                  Reveja todas as informações antes de enviar a mensagem
							  </div>

                <form className="com-mail" method="post" action="c_nova_mensagem.php">
                  <input type="text" name="destinatario" value="Nome de usuário do destinatário" />
                  <textarea rows="6" name="nova_mensagem" value="Mensagem">Mensagem</textarea>

                  <input type="submit" value="Enviar Mensagem" />
                </form>
              </div>
            </div>
          </div>

          <div className="clearfix"> </div>
        </div>
      </div>
    </Container>
  );
}