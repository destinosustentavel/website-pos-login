import React from 'react';

import Container from '../components/Container';

import '../styles/details.css';

import papelIcon from '../assets/images/papel.png';

export default function Details() {
  return (
    <Container>
      <div className="details-container">
        <div className="container-top">
          <div className="left-side">
            <h2 className="title">Oferta</h2>

            <div className="content">
              <span className="material">Material: Jornais</span>
              <span className="material">Quantidade: 5 quilos</span>
              <span className="material">Status: Disponível</span>
              <span className="material">Operação: Venda</span>
              <span className="material">Dia de coleta: Segunda</span>
              <span className="material">Horário: 09:00 até 11:00 horas</span>
              <span className="material">Ofertado por: Luciano Teran</span>
              <span className="material">Cidade: Castanhal/PA</span>
            </div>
          </div>

          <div className="right-side">
            <img src={papelIcon} alt="Material" />

            <div className="buttons-container">
              <button>Eu Quero!</button>
              <button>Enviar Mensagem</button>
            </div>
          </div>
        </div>

        <div className="container-bottom"></div>
      </div>
    </Container>
  );
}