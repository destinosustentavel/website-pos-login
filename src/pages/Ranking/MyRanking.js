import React from 'react';

import Container from '../../components/Container';

export default function MyRanking() {
  return (
    <Container>
      <div class="inner-block">
        <div class="blank">
          <h2>Minha Posição</h2>
          <div class="col-md-12 chit-chat-layer1-left">
            <div class="work-progres">
              <div class="table-responsive">
                <table class="table table-hover">
                  <thead>
                    <tr>
                      <th>POSIÇÃO</th>
                      <th>USUÁRIO</th>
                      <th>PONTUAÇÃO</th>
                      <th>OFERTAS</th>
                      <th>PEDIDOS</th>
                    </tr>
                  </thead>
                  <tbody>
                    {/* ... */}
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
        <div class="clearfix"> </div>
      </div>
    </Container >
  );
}