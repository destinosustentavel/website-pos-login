import React from 'react';

import Container from '../../components/Container';

export default function RankingCategory() {
  return (
    <Container>
      <div class="inner-block">
        <div class="blank">
          <h2>Minha Posição</h2>
          <div class="col-md-12 chit-chat-layer1-left">
            <div class="work-progres">
              <div class="agileits_main_grid_left_l_grids">
                <div class="tipo_pessoa_ranking">
                  <div class="w3_agileits_main_grid_left_l">
                    <h3>Tipo de Pessoa: <span class="wthree_color">*</span></h3>
                  </div>

                  <div class="agileinfo_radio_button">
                    <label class="radio"><input type="radio" name="radio" value="F" onchange="AtivCampFisica()" /><i></i>Física</label>
                  </div>

                  <div class="agileinfo_radio_button">
                    <label class="radio"><input type="radio" name="radio" value="J" onchange="AtivCampJuridica()" /><i></i>Jurídica</label>
                  </div>
                </div>

                <div id="p_fisica" class="invisivel">
                  <select name="tipo_user" id="w3_agileits_select" class="w3layouts_select" onchange="user_fisico(this.value)">
                    <option value="" disabled selected>Tipo de Usuário</option>
                    <option value="ar">Artesão</option>
                    <option value="ca">Catador</option>
                    <option value="co">Colaborador</option>
                  </select>
                </div>

                <div id="p_juridica" class="invisivel">
                  <select name="jtipo_user" id="w3_agileits_select" class="w3layouts_select" onchange="user_juridico(this.value)">
                    <option value="" disabled selected>Tipo de Usuário</option>
                    <option value="as">Associação de Catadores</option>
                    <option value="co">Cooperativa de Reciclagem</option>
                    <option value="em">Empresa</option>
                    <option value="in">Indústria</option>
                    <option value="is">Instituição de Ensino</option>
                    {/* FALTA FAZER */}
                  </select>
                </div>

                <table class="table table-hover">
                  <thead>
                    <tr>
                      <th>POSIÇÃO</th>
                      <th>USUÁRIO</th>
                      <th>PONTUAÇÂO</th>
                      <th>OFERTAS</th>
                      <th>PEDIDOS</th>
                    </tr>
                  </thead>

                  {/* Pessoa física */}
                  <tbody class="invisivel" id="tb_artesao">
                    {/* ... */}
                  </tbody>

                  <tbody class="invisivel" id="tb_catador">
                    {/* ... */}
                  </tbody>

                  <tbody class="invisivel" id="tb_colaborador">
                    {/* ... */}
                  </tbody>

                  {/* Pessoa Jurídica */}
                  <tbody class="invisivel" id="tb_cooperativa">
                    {/* ... */}
                  </tbody>

                  <tbody class="invisivel" id="tb_associacao">
                    {/* ... */}
                  </tbody>

                  <tbody class="invisivel" id="tb_empresa">
                    {/* ... */}
                  </tbody>

                  <tbody class="invisivel" id="tb_industria">
                    {/* ... */}
                  </tbody>

                  {/* falta fazer */}
                  <tbody class="invisivel" id="tb_instituicao_ensino">
                    {/* ... */}
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>

        <div class="clearfix"> </div>
      </div>
    </Container>
  );
}