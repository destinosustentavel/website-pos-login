import React from 'react';

import Container from '../components/Container';

export default function Home() {
  return (
    <Container>
      <div className="inner-block">
        <div className="market-updates">
          <div className="col-md-4 market-update-gd">
            <div
              className="market-update-block clr-block-1"
              onClick={() => console.log('ranking minha posicao')}
            >
              <div className="col-md-8 market-update-left">
                <h3>200</h3>
                <h4>Pontuação</h4>
              </div>
              <div className="col-md-4 market-update-right">
                <i className="fa fa-star-half-o"> </i>
              </div>
              <div className="clearfix"></div>
            </div>
          </div>

          <div className="col-md-4 market-update-gd">
            <div
              className="market-update-block clr-block-2"
              onClick={() => console.log('minhas ofertas')}
            >
              <div className="col-md-8 market-update-left">
                <h3>10</h3>
                <h4>Ofertas</h4>
              </div>
              <div className="col-md-4 market-update-right">
                <i className="fa fa-shopping-basket"> </i>
              </div>
              <div className="clearfix"> </div>
            </div>
          </div>

          <div className="col-md-4 market-update-gd">
            <div
              className="market-update-block clr-block-3"
              onClick={() => console.log('meus pedidos')}
            >
              <div className="col-md-8 market-update-left">
                <h3>12</h3>
                <h4>Pedidos</h4>
              </div>
              <div className="col-md-4 market-update-right">
                <i className="fa fa-cart-plus"> </i>
              </div>
              <div className="clearfix"> </div>
            </div>
          </div>

          <div className="clearfix"> </div>
        </div>

        {/* mainpage chit-chating */}
        <div className="chit-chat-layer1">
          <div className="col-md-6 chart-layer1-left">
            <div className="user-marorm">
              <div className="malorum-top"></div>
              <div className="malorm-bottom">
                <span className="malorum-pro" id="ImagemPerf"> </span>
                <h4>Pessoa Física</h4>
                <h2>Usuário</h2>
              </div>
            </div>
          </div>

          <div className="col-md-6 chit-chat-layer1-rit">
            <div className="geo-chart">
              <section id="charts1" className="charts">
                <div className="wrapper-flex">
                  <div className="col geo_main">
                    <h3 id="geoChartTitle">Atividades dos estados brasileiros</h3>
                    <div id="geoChart" className="chart"> </div>
                  </div>
                </div>
              </section>
            </div>
          </div>
          <div className="clearfix"> </div>
        </div>
        {/* main page chit chating end here */}

        {/* main page chart start here */}
        <div className="main-page-charts">
          <div className="main-page-chart-layer1">
            <div className="col-md-6 chart-layer1-left">
              <div className="glocy-chart">
                <div className="span-2c">
                  <h3 className="tlt">Análise das atividades com Plásticos</h3>
                  <canvas id="bar" height="300" width="400" style={{ width: 400 + 'px', height: 300 + 'px' }}></canvas>
                </div>
              </div>
            </div>

            <div className="col-md-6 chart-layer1-left">
              <div className="glocy-chart">
                <div className="span-2c">
                  <h3 className="tlt">Análise das atividades com Metais</h3>
                  <canvas id="bar2" height="300" width="400" style={{ width: 400 + 'px', height: 300 + 'px' }}></canvas>
                </div>
              </div>
            </div>

            <div className="col-md-6 chart-layer1-left">
              <div className="glocy-chart">
                <div className="span-2c">
                  <h3 className="tlt">Análise das atividades com Papéis</h3>
                  <canvas id="bar3" height="300" width="400" style={{ width: 400 + 'px', height: 300 + 'px' }}></canvas>
                </div>
              </div>
            </div>

            <div className="col-md-6 chart-layer1-right">
              <div className="glocy-chart">
                <div className="span-2c">
                  <h3 className="tlt">Análise das atividades com Vidros</h3>
                  <canvas id="bar4" height="300" width="400" style={{ width: 400 + 'px', height: 300 + 'px' }}></canvas>
                </div>
              </div>
            </div>

            <div className="col-md-6 chart-layer1-left">
              <div className="glocy-chart">
                <div className="span-2c">
                  <h3 className="tlt">Análise das atividades com Eletrônicos</h3>
                  <canvas id="bar5" height="300" width="400" style={{ width: 400 + 'px', height: 300 + 'px' }}></canvas>
                </div>
              </div>
            </div>

            <div className="clearfix"> </div>
          </div>
        </div>

        {/* main page chart layer2 */}
        <div className="chart-layer-2">
          <div className="col-md-6 chart-layer1-right">
            <div className="prograc-blocks">
              {/* Progress bars */}
              <div className="home-progres-main">
                <h3>Número total de ofertas e pedidos</h3>
              </div>
              <div className='bar_group'></div>
            </div>
          </div>
          <div className="clearfix"> </div>
        </div>
      </div>
    </Container>
  );
}