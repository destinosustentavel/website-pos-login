import React from 'react';

import Container from '../../components/Container';
import Card from '../../components/Card';

export default function Demand() {
  return (
    <Container>
      <div className="inner-block">
        <div className="product-block">
          <div className="pro-head">
            <h2>Meus Pedidos</h2>

            <div className="cards-container">
              <Card />
            </div>
          </div>

          <div className="clearfix"> </div>
        </div>
      </div>
    </Container>
  );
}