import React from 'react';
import { Link } from 'react-router-dom';

import '../styles/card.css';

import papelIcon from '../assets/images/papel.png';

export default function Card() {
  return (
    <div className="card-container">
      <h3>Pedido</h3>

      <img src={papelIcon} alt="Material"/>

      <div className="card-content">
        <span className="material-type">Jornais</span>
        <span className="material-amount">5 quilos</span>
        <span className="status">Disponível para venda</span>
        <span className="location">Castanhal</span>
      </div>

      <Link to="/detalhes" className="confirm-button">
        <p>Eu Quero!</p>
      </Link>
    </div>
  );
}