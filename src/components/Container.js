import React from 'react';

import Header from './Header';
import Footer from './Footer';
import Menu from './Menu';

import '../assets/css/bootstrap.css';
import '../assets/css/style.css';
import '../assets/css/font-awesome.css';

export default function Container({ children }) {
  return (
    <div className="page-container">
      <div className="left-content">
        <div className="mother-grid-inner">
          <Header />

          {children}

          <Footer />
        </div>
      </div>

      <Menu />
    </div>
  );
}