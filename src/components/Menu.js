import React from 'react';

export default function Menu() {
  return (
    <div className="sidebar-menu">
      <div className="logo">
        <a href="#" className="sidebar-icon">
          <span className="fa fa-bars"></span>
        </a>
        <a href="#">
          <span id="logo"></span>
        </a>
      </div>

      <div className="menu">
        <ul id="menu">
          <li id="menu-home">
            <a href="/">
              <i className="fa fa-home"></i>
              <span>Início</span>
            </a>
          </li>

          <li>
            <a href="/feed">
              <i className="fa fa-rss-square"></i>
              <span>Feed</span>
            </a>
          </li>

          <li>
            <a href="/notificacoes">
              <i className="fa fa-bell-o"></i>
              <span>Notificações</span>
            </a>
          </li>

          {/* <li>
            <a href="#">
              <i className="fa fa-envelope"></i>
              <span>Mensagens</span>
              <span className="fa fa-angle-right" style={{ float: 'right' }}></span>
            </a>
            <ul id="menu-academico-sub">
              <li id="menu-academico-boletim"><a href="/minhas-mensagens">Minhas Mensagens</a></li>
              <li id="menu-academico-boletim"><a href="/nova-conversa">Nova Conversa</a></li>
            </ul>
          </li> */}

          <li id="menu-comunicacao">
            <a href="#">
              <i className="fa fa-shopping-cart"></i>
              <span>Pedidos</span>
              <span className="fa fa-angle-right" style={{ float: 'right' }}>
              </span>
            </a>
            <ul id="menu-comunicacao-sub">
              <li id="menu-mensagens"><a href="/meus-pedidos">Meus Pedidos</a></li>
              <li id="menu-arquivos"><a href="/novo-pedido">Novo pedido</a></li>
              <li id="menu-arquivos"><a href="/pedidos-atendidos">Pedidos que atendi</a></li>
            </ul>
          </li>

          <li id="menu-academico">
            <a href="#">
              <i className="fa fa-shopping-bag"></i>
              <span>Ofertas</span>
              <span className="fa fa-angle-right" style={{ float: 'right' }}>
              </span>
            </a>
            <ul id="menu-academico-sub">
              <li id="menu-academico-boletim"><a href="/minhas-ofertas">Minhas Ofertas</a></li>
              <li id="menu-academico-avaliacoes"><a href="/nova-oferta">Nova Oferta</a></li>
              <li id="menu-academico-avaliacoes"><a href="/ofertas-atendidas">Ofertas que atendi</a></li>
            </ul>
          </li>

          <li>
            <a href="#">
              <i className="fa fa-bar-chart"></i>
              <span>Ranking</span>
              <span className="fa fa-angle-right" style={{ float: 'right' }}></span>
            </a>
            <ul id="menu-academico-sub">
              <li id="menu-academico-avaliacoes"><a href="/minha-posicao">Minha Posição</a></li>
              <li id="menu-academico-boletim"><a href="/ranking-categoria">Ranking por Categoria</a></li>
            </ul>
          </li>
        </ul>
      </div>
    </div>
  );
}