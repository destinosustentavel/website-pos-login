import React from 'react';

export default function Footer() {
  return (
    <div className="copyrights">
      <p>© 2020 Destino Sustentável. All Rights Reserved | Design by  <a href="http://w3layouts.com/" target="_blank" rel="noopener noreferrer">W3layouts</a> </p>
    </div>
  );
}