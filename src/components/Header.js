import React from 'react';

export default function Header() {
  return (
    <div className="header-main">
      <div className="header-left">
        <div className="logo-name">
          <a href="#">
            <h2>Destino Sustentável</h2>
          </a>
        </div>

        <div className="search-box">
          <form action="pesquisar1.php" method="post">
            <input type="text" placeholder="Pesquisar..." name="pesquisa" />
            <input type="submit" value="" />
          </form>
        </div>

        <div className="clearfix"> </div>
      </div>

      <div className="header-right">
        <div className="profile_details_left">
          <ul className="nofitications-dropdown">
            <li className="dropdown head-dpdn" onClick={() => console.log('minhas_msgs')}>
              <a href="#" className="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i className="fa fa-envelope"></i></a>
            </li>

            <li className="dropdown head-dpdn" onClick={() => console.log('notificacoes')}>
              <a href="#" className="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i className="fa fa-bell"></i><span className="badge blue">10</span></a>
            </li>

            <li className="dropdown head-dpdn">
              <a href="#" className="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i className="fa fa-tasks"></i><span className="badge blue1">20</span></a>

              <ul className="dropdown-menu">
                <li>
                  <div className="notification_header">
                    <h3>Produtos Doados</h3>
                  </div>
                </li>

                <li>
                  <a href="#">
                    <div className="task-info">
                      <span className="task-desc">Metal</span>
                      <span className="percentage">0%</span>

                      <div className="clearfix"></div>
                    </div>
                    <div className="progress progress-striped active">
                      {/* barra de acordo com a porcentagem do material */}
                      <div className="bar yellow" id="metal" style={{ width: 40 + '%' }}>
                      </div>
                    </div>
                  </a>
                </li>

                <li>
                  <a href="#">
                    <div className="task-info">
                      <span className="task-desc">Vidro</span>
                      <span className="percentage">0%</span>

                      <div className="clearfix"></div>
                    </div>
                    <div className="progress progress-striped active">
                      {/* barra de acordo com a porcentagem do material */}
                      <div className="bar green" id="vidro" style={{ width: 33 + '%' }}></div>
                    </div>
                  </a>
                </li>

                <li>
                  <a href="#">
                    <div className="task-info">
                      <span className="task-desc">Papel</span>
                      <span className="percentage">0%</span>

                      <div className="clearfix"></div>
                    </div>
                    <div className="progress progress-striped active">
                      {/* barra de acordo com a porcentagem do material */}
                      <div className="bar red" id="papel" style={{ width: 33 + '%' }}>
                      </div>
                    </div>
                  </a>
                </li>

                <li>
                  <a href="#">
                    <div className="task-info">
                      <span className="task-desc">Plastico</span>
                      <span className="percentage">0%</span>

                      <div className="clearfix"></div>
                    </div>
                    <div className="progress progress-striped active">
                      {/* barra de acordo com a porcentagem do material */}
                      <div className="bar  blue" id="plastico" style={{ width: 80 + '%' }}>
                      </div>
                    </div>
                  </a>
                </li>

                <li>
                  <a href="#">
                    <div className="task-info">
                      <span className="task-desc">Eletrônico</span>
                      <span className="percentage">0%</span>

                      <div className="clearfix"></div>
                    </div>
                    <div className="progress progress-striped active">
                      {/* barra de acordo com a porcentagem do material */}
                      <div className="bar  blue" id="eletronico" style={{ width: 80 + '%' }}>
                      </div>
                    </div>
                  </a>
                </li>

                <li>
                  <a href="#">
                    <div className="task-info">
                      <span className="task-desc">Outros</span>
                      <span className="percentage">0%</span>

                      <div className="clearfix"></div>
                    </div>

                    <div className="progress progress-striped active">
                      {/* barra de acordo com a porcentagem do material */}
                      <div className="bar  blue" id="outros" style={{ width: 80 + '%' }}></div>
                    </div>
                  </a>
                </li>

                <li>
                  <div className="notification_bottom">
                    <a href="#">Todos os produtos</a>
                  </div>
                </li>
              </ul>
            </li>
          </ul>

          <div className="clearfix"> </div>
        </div>

        <div className="profile_details">
          <ul>
            <li className="dropdown profile_details_drop">
              <a href="#" className="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                <div className="profile_img">
                  <span className="prfil-img">
                    <img src="" id="imagem_perfil" alt="" />
                  </span>

                  <div className="user-name">
                    <p>Usuário</p>
                    <span>Pessoa Física</span>
                  </div>

                  <i className="fa fa-angle-down lnr"></i>
                  <i className="fa fa-angle-up lnr"></i>
                  <div className="clearfix"></div>
                </div>
              </a>

              <ul className="dropdown-menu drp-mnu">
                <li><a href="configuracoes.php"><i className="fa fa-cog"></i> Configurações</a></li>
                <li><a href="meu_perfil.php"><i className="fa fa-user"></i> Perfil</a></li>
                <li><a href="logoff.php"><i className="fa fa-sign-out"></i> SAIR</a></li>
              </ul>
            </li>
          </ul>
        </div>

        <div className="clearfix"> </div>
      </div>

      <div className="clearfix"> </div>
    </div>
  );
}