# Pós Login

Pós login do website do Destino Sustentável.

## Ferramentas utilizadas

* [ReactJS](https://pt-br.reactjs.org/)
* [Yarn](https://yarnpkg.com/)

## Como utilizar

```bash
# clone este repositório
git clone https://gitlab.com/destinosustentavel/website-pos-login.git

# instale as dependências
npm install / yarn install

# execute a aplicação
npm start / yarn start
```